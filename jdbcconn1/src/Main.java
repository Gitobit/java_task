import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
​
public class Main {
    public static void main(String args[]) {
        Connection c = null;
        List<User> users = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/spring",
                            "spring", "root");
            PreparedStatement statement = c.prepareStatement("SELECT * FROM Employee;");
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                User user = new User();
                user.id = result.getInt("user_id");
                user.firstName = result.getString("firstName");
                user.lastName = result.getString("lastName");
            }​
            for (User user: users) {
                System.out.println(user);
            }
            System.out.println("Connected");
​
            result.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }
}
​
class User{
    public int id;
    public String firstName;
    public String lastName;
​
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", laName='" + lastName + '\'' +
                '}';
    }
}
​