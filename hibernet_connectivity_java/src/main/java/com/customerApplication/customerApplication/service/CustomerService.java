package com.customerApplication.customerApplication.service;

import com.customerApplication.customerApplication.dao.CustomerDAO;
import com.customerApplication.customerApplication.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CustomerService {

    @Autowired
    private CustomerDAO customerDAO;

    public Customer addCustomer(Customer customer){
        return customerDAO.save(customer);
    }

    public List<Customer> getCustomer(){
        return customerDAO.findAll();
    }

    public Customer getCustomer(int customerId){
        return customerDAO.findById(customerId).get();
    }

    public void deleteCustomer(int id){
        customerDAO.deleteById(id);
    }

}


